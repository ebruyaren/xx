package com.app.derin.uaa.service.impl;

import com.app.derin.uaa.service.UaaScreensService;
import com.app.derin.uaa.domain.UaaScreens;
import com.app.derin.uaa.repository.UaaScreensRepository;
import com.app.derin.uaa.service.dto.UaaScreensDTO;
import com.app.derin.uaa.service.mapper.UaaScreensMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UaaScreens}.
 */
@Service
@Transactional
public class UaaScreensServiceImpl implements UaaScreensService {

    private final Logger log = LoggerFactory.getLogger(UaaScreensServiceImpl.class);

    private final UaaScreensRepository uaaScreensRepository;

    private final UaaScreensMapper uaaScreensMapper;

    public UaaScreensServiceImpl(UaaScreensRepository uaaScreensRepository, UaaScreensMapper uaaScreensMapper) {
        this.uaaScreensRepository = uaaScreensRepository;
        this.uaaScreensMapper = uaaScreensMapper;
    }

    /**
     * Save a uaaScreens.
     *
     * @param uaaScreensDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UaaScreensDTO save(UaaScreensDTO uaaScreensDTO) {
        log.debug("Request to save UaaScreens : {}", uaaScreensDTO);
        UaaScreens uaaScreens = uaaScreensMapper.toEntity(uaaScreensDTO);
        uaaScreens = uaaScreensRepository.save(uaaScreens);
        return uaaScreensMapper.toDto(uaaScreens);
    }

    /**
     * Get all the uaaScreens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UaaScreensDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UaaScreens");
        return uaaScreensRepository.findAll(pageable)
            .map(uaaScreensMapper::toDto);
    }

    /**
     * Get all the uaaScreens with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<UaaScreensDTO> findAllWithEagerRelationships(Pageable pageable) {
        return uaaScreensRepository.findAllWithEagerRelationships(pageable).map(uaaScreensMapper::toDto);
    }
    

    /**
     * Get one uaaScreens by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UaaScreensDTO> findOne(Long id) {
        log.debug("Request to get UaaScreens : {}", id);
        return uaaScreensRepository.findOneWithEagerRelationships(id)
            .map(uaaScreensMapper::toDto);
    }

    /**
     * Delete the uaaScreens by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UaaScreens : {}", id);
        uaaScreensRepository.deleteById(id);
    }
}
