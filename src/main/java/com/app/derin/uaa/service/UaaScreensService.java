package com.app.derin.uaa.service;

import com.app.derin.uaa.service.dto.UaaScreensDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.app.derin.uaa.domain.UaaScreens}.
 */
public interface UaaScreensService {

    /**
     * Save a uaaScreens.
     *
     * @param uaaScreensDTO the entity to save.
     * @return the persisted entity.
     */
    UaaScreensDTO save(UaaScreensDTO uaaScreensDTO);

    /**
     * Get all the uaaScreens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UaaScreensDTO> findAll(Pageable pageable);

    /**
     * Get all the uaaScreens with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<UaaScreensDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" uaaScreens.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UaaScreensDTO> findOne(Long id);

    /**
     * Delete the "id" uaaScreens.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
