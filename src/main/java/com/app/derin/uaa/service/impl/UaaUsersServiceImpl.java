package com.app.derin.uaa.service.impl;

import com.app.derin.uaa.service.UaaUsersService;
import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.repository.UaaUsersRepository;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import com.app.derin.uaa.service.mapper.UaaUsersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UaaUsers}.
 */
@Service
@Transactional
public class UaaUsersServiceImpl implements UaaUsersService {

    private final Logger log = LoggerFactory.getLogger(UaaUsersServiceImpl.class);

    private final UaaUsersRepository uaaUsersRepository;

    private final UaaUsersMapper uaaUsersMapper;

    public UaaUsersServiceImpl(UaaUsersRepository uaaUsersRepository, UaaUsersMapper uaaUsersMapper) {
        this.uaaUsersRepository = uaaUsersRepository;
        this.uaaUsersMapper = uaaUsersMapper;
    }

    /**
     * Save a uaaUsers.
     *
     * @param uaaUsersDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UaaUsersDTO save(UaaUsersDTO uaaUsersDTO) {
        log.debug("Request to save UaaUsers : {}", uaaUsersDTO);
        UaaUsers uaaUsers = uaaUsersMapper.toEntity(uaaUsersDTO);
        uaaUsers = uaaUsersRepository.save(uaaUsers);
        return uaaUsersMapper.toDto(uaaUsers);
    }

    /**
     * Get all the uaaUsers.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UaaUsersDTO> findAll() {
        log.debug("Request to get all UaaUsers");
        return uaaUsersRepository.findAll().stream()
            .map(uaaUsersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one uaaUsers by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UaaUsersDTO> findOne(Long id) {
        log.debug("Request to get UaaUsers : {}", id);
        return uaaUsersRepository.findById(id)
            .map(uaaUsersMapper::toDto);
    }

    /**
     * Delete the uaaUsers by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UaaUsers : {}", id);
        uaaUsersRepository.deleteById(id);
    }
}
