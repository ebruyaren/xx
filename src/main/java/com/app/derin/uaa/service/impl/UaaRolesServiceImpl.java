package com.app.derin.uaa.service.impl;

import com.app.derin.uaa.service.UaaRolesService;
import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.repository.UaaRolesRepository;
import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.service.mapper.UaaRolesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UaaRoles}.
 */
@Service
@Transactional
public class UaaRolesServiceImpl implements UaaRolesService {

    private final Logger log = LoggerFactory.getLogger(UaaRolesServiceImpl.class);

    private final UaaRolesRepository uaaRolesRepository;

    private final UaaRolesMapper uaaRolesMapper;

    public UaaRolesServiceImpl(UaaRolesRepository uaaRolesRepository, UaaRolesMapper uaaRolesMapper) {
        this.uaaRolesRepository = uaaRolesRepository;
        this.uaaRolesMapper = uaaRolesMapper;
    }

    /**
     * Save a uaaRoles.
     *
     * @param uaaRolesDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UaaRolesDTO save(UaaRolesDTO uaaRolesDTO) {
        log.debug("Request to save UaaRoles : {}", uaaRolesDTO);
        UaaRoles uaaRoles = uaaRolesMapper.toEntity(uaaRolesDTO);
        uaaRoles = uaaRolesRepository.save(uaaRoles);
        return uaaRolesMapper.toDto(uaaRoles);
    }

    /**
     * Get all the uaaRoles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UaaRolesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UaaRoles");
        return uaaRolesRepository.findAll(pageable)
            .map(uaaRolesMapper::toDto);
    }

    /**
     * Get all the uaaRoles with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<UaaRolesDTO> findAllWithEagerRelationships(Pageable pageable) {
        return uaaRolesRepository.findAllWithEagerRelationships(pageable).map(uaaRolesMapper::toDto);
    }
    

    /**
     * Get one uaaRoles by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UaaRolesDTO> findOne(Long id) {
        log.debug("Request to get UaaRoles : {}", id);
        return uaaRolesRepository.findOneWithEagerRelationships(id)
            .map(uaaRolesMapper::toDto);
    }

    /**
     * Delete the uaaRoles by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UaaRoles : {}", id);
        uaaRolesRepository.deleteById(id);
    }
}
