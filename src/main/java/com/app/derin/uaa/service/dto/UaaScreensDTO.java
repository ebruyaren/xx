package com.app.derin.uaa.service.dto;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.app.derin.uaa.domain.UaaScreens} entity.
 */
public class UaaScreensDTO implements Serializable {

    private Long id;

    private String screenName;


    private Set<UaaRolesDTO> roles = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Set<UaaRolesDTO> getRoles() {
        return roles;
    }

    public void setRoles(Set<UaaRolesDTO> uaaRoles) {
        this.roles = uaaRoles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UaaScreensDTO uaaScreensDTO = (UaaScreensDTO) o;
        if (uaaScreensDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), uaaScreensDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UaaScreensDTO{" +
            "id=" + getId() +
            ", screenName='" + getScreenName() + "'" +
            "}";
    }
}
