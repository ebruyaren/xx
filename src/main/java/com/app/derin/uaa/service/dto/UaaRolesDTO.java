package com.app.derin.uaa.service.dto;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.app.derin.uaa.domain.UaaRoles} entity.
 */
public class UaaRolesDTO implements Serializable {

    private Long id;

    private String roleName;


    private Set<UaaUsersDTO> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<UaaUsersDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UaaUsersDTO> uaaUsers) {
        this.users = uaaUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UaaRolesDTO uaaRolesDTO = (UaaRolesDTO) o;
        if (uaaRolesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), uaaRolesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UaaRolesDTO{" +
            "id=" + getId() +
            ", roleName='" + getRoleName() + "'" +
            "}";
    }
}
