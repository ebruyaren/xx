package com.app.derin.uaa.ext.roles;

import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.service.mapper.UaaRolesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UaaRolesBusServiceImpl implements UaaRolesBusService {
     private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.roles.UaaRolesBusServiceImpl.class);

     private final UaaRolesBusRepository uaaRolesBusRepository;
     private final UaaRolesMapper uaaRolesMapper;

    public UaaRolesBusServiceImpl(UaaRolesBusRepository uaaRolesBusRepository, UaaRolesMapper uaaRolesMapper) {
        this.uaaRolesBusRepository = uaaRolesBusRepository;
        this.uaaRolesMapper = uaaRolesMapper;
    }

    @Override
    public UaaRolesDTO save(UaaRolesDTO uaaRolesDTO) {
        log.debug("Request to save UaaRoles : {}", uaaRolesDTO);
        UaaRoles uaaRoles = uaaRolesMapper.toEntity(uaaRolesDTO);
        uaaRoles = uaaRolesBusRepository.save(uaaRoles);
        return uaaRolesMapper.toDto(uaaRoles);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UaaRolesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UaaRoles {}", pageable);
//        return (Page<UaaRolesDTO>) uaaRolesBusRepository.getAll();
        return uaaRolesBusRepository.getAll(pageable)
            .map(uaaRolesMapper::toDto);
    }

    @Override
    public UaaRolesDTO findOne(Long id) {
        return uaaRolesMapper.toDto(uaaRolesBusRepository.getOne(id));
//        return uaaRolesBusRepository.getOne(id);
    }

    @Override
    public void delete(Long id) {
        uaaRolesBusRepository.deleteById(id);

    }
}
