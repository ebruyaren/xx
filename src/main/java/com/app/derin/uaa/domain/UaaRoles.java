package com.app.derin.uaa.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UaaRoles.
 */
@Entity
@Table(name = "uaa_roles")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UaaRoles implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "role_name")
    private String roleName;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "uaa_roles_users",
               joinColumns = @JoinColumn(name = "uaa_roles_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"))
    private Set<UaaUsers> users = new HashSet<>();

    @ManyToMany(mappedBy = "roles")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<UaaScreens> screens = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public UaaRoles roleName(String roleName) {
        this.roleName = roleName;
        return this;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<UaaUsers> getUsers() {
        return users;
    }

    public UaaRoles users(Set<UaaUsers> uaaUsers) {
        this.users = uaaUsers;
        return this;
    }

    public UaaRoles addUsers(UaaUsers uaaUsers) {
        this.users.add(uaaUsers);
        uaaUsers.getRoles().add(this);
        return this;
    }

    public UaaRoles removeUsers(UaaUsers uaaUsers) {
        this.users.remove(uaaUsers);
        uaaUsers.getRoles().remove(this);
        return this;
    }

    public void setUsers(Set<UaaUsers> uaaUsers) {
        this.users = uaaUsers;
    }

    public Set<UaaScreens> getScreens() {
        return screens;
    }

    public UaaRoles screens(Set<UaaScreens> uaaScreens) {
        this.screens = uaaScreens;
        return this;
    }

    public UaaRoles addScreens(UaaScreens uaaScreens) {
        this.screens.add(uaaScreens);
        uaaScreens.getRoles().add(this);
        return this;
    }

    public UaaRoles removeScreens(UaaScreens uaaScreens) {
        this.screens.remove(uaaScreens);
        uaaScreens.getRoles().remove(this);
        return this;
    }

    public void setScreens(Set<UaaScreens> uaaScreens) {
        this.screens = uaaScreens;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UaaRoles)) {
            return false;
        }
        return id != null && id.equals(((UaaRoles) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UaaRoles{" +
            "id=" + getId() +
            ", roleName='" + getRoleName() + "'" +
            "}";
    }
}
