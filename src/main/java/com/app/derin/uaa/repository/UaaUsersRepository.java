package com.app.derin.uaa.repository;

import com.app.derin.uaa.domain.UaaUsers;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UaaUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UaaUsersRepository extends JpaRepository<UaaUsers, Long> {

}
