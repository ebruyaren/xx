package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.service.UaaRolesService;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import com.app.derin.uaa.service.dto.UaaRolesDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.app.derin.uaa.domain.UaaRoles}.
 */
@RestController
@RequestMapping("/api")
public class UaaRolesResource {

    private final Logger log = LoggerFactory.getLogger(UaaRolesResource.class);

    private static final String ENTITY_NAME = "uaaRoles";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaRolesService uaaRolesService;

    public UaaRolesResource(UaaRolesService uaaRolesService) {
        this.uaaRolesService = uaaRolesService;
    }

    /**
     * {@code POST  /uaa-roles} : Create a new uaaRoles.
     *
     * @param uaaRolesDTO the uaaRolesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uaaRolesDTO, or with status {@code 400 (Bad Request)} if the uaaRoles has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/uaa-roles")
    public ResponseEntity<UaaRolesDTO> createUaaRoles(@RequestBody UaaRolesDTO uaaRolesDTO) throws URISyntaxException {
        log.debug("REST request to save UaaRoles : {}", uaaRolesDTO);
        if (uaaRolesDTO.getId() != null) {
            throw new BadRequestAlertException("A new uaaRoles cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaRolesDTO result = uaaRolesService.save(uaaRolesDTO);
        return ResponseEntity.created(new URI("/api/uaa-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /uaa-roles} : Updates an existing uaaRoles.
     *
     * @param uaaRolesDTO the uaaRolesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uaaRolesDTO,
     * or with status {@code 400 (Bad Request)} if the uaaRolesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uaaRolesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/uaa-roles")
    public ResponseEntity<UaaRolesDTO> updateUaaRoles(@RequestBody UaaRolesDTO uaaRolesDTO) throws URISyntaxException {
        log.debug("REST request to update UaaRoles : {}", uaaRolesDTO);
        if (uaaRolesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaRolesDTO result = uaaRolesService.save(uaaRolesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaRolesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /uaa-roles} : get all the uaaRoles.
     *

     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uaaRoles in body.
     */
    @GetMapping("/uaa-roles")
    public ResponseEntity<List<UaaRolesDTO>> getAllUaaRoles(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of UaaRoles");
        Page<UaaRolesDTO> page;
        if (eagerload) {
            page = uaaRolesService.findAllWithEagerRelationships(pageable);
        } else {
            page = uaaRolesService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /uaa-roles/:id} : get the "id" uaaRoles.
     *
     * @param id the id of the uaaRolesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uaaRolesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/uaa-roles/{id}")
    public ResponseEntity<UaaRolesDTO> getUaaRoles(@PathVariable Long id) {
        log.debug("REST request to get UaaRoles : {}", id);
        Optional<UaaRolesDTO> uaaRolesDTO = uaaRolesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uaaRolesDTO);
    }

    /**
     * {@code DELETE  /uaa-roles/:id} : delete the "id" uaaRoles.
     *
     * @param id the id of the uaaRolesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/uaa-roles/{id}")
    public ResponseEntity<Void> deleteUaaRoles(@PathVariable Long id) {
        log.debug("REST request to delete UaaRoles : {}", id);
        uaaRolesService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
