package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.service.UaaScreensService;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import com.app.derin.uaa.service.dto.UaaScreensDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.app.derin.uaa.domain.UaaScreens}.
 */
@RestController
@RequestMapping("/api")
public class UaaScreensResource {

    private final Logger log = LoggerFactory.getLogger(UaaScreensResource.class);

    private static final String ENTITY_NAME = "uaaScreens";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaScreensService uaaScreensService;

    public UaaScreensResource(UaaScreensService uaaScreensService) {
        this.uaaScreensService = uaaScreensService;
    }

    /**
     * {@code POST  /uaa-screens} : Create a new uaaScreens.
     *
     * @param uaaScreensDTO the uaaScreensDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uaaScreensDTO, or with status {@code 400 (Bad Request)} if the uaaScreens has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/uaa-screens")
    public ResponseEntity<UaaScreensDTO> createUaaScreens(@RequestBody UaaScreensDTO uaaScreensDTO) throws URISyntaxException {
        log.debug("REST request to save UaaScreens : {}", uaaScreensDTO);
        if (uaaScreensDTO.getId() != null) {
            throw new BadRequestAlertException("A new uaaScreens cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaScreensDTO result = uaaScreensService.save(uaaScreensDTO);
        return ResponseEntity.created(new URI("/api/uaa-screens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /uaa-screens} : Updates an existing uaaScreens.
     *
     * @param uaaScreensDTO the uaaScreensDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uaaScreensDTO,
     * or with status {@code 400 (Bad Request)} if the uaaScreensDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uaaScreensDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/uaa-screens")
    public ResponseEntity<UaaScreensDTO> updateUaaScreens(@RequestBody UaaScreensDTO uaaScreensDTO) throws URISyntaxException {
        log.debug("REST request to update UaaScreens : {}", uaaScreensDTO);
        if (uaaScreensDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaScreensDTO result = uaaScreensService.save(uaaScreensDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaScreensDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /uaa-screens} : get all the uaaScreens.
     *

     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uaaScreens in body.
     */
    @GetMapping("/uaa-screens")
    public ResponseEntity<List<UaaScreensDTO>> getAllUaaScreens(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of UaaScreens");
        Page<UaaScreensDTO> page;
        if (eagerload) {
            page = uaaScreensService.findAllWithEagerRelationships(pageable);
        } else {
            page = uaaScreensService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /uaa-screens/:id} : get the "id" uaaScreens.
     *
     * @param id the id of the uaaScreensDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uaaScreensDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/uaa-screens/{id}")
    public ResponseEntity<UaaScreensDTO> getUaaScreens(@PathVariable Long id) {
        log.debug("REST request to get UaaScreens : {}", id);
        Optional<UaaScreensDTO> uaaScreensDTO = uaaScreensService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uaaScreensDTO);
    }

    /**
     * {@code DELETE  /uaa-screens/:id} : delete the "id" uaaScreens.
     *
     * @param id the id of the uaaScreensDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/uaa-screens/{id}")
    public ResponseEntity<Void> deleteUaaScreens(@PathVariable Long id) {
        log.debug("REST request to delete UaaScreens : {}", id);
        uaaScreensService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
