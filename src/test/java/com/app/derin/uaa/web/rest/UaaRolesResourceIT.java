package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.AuthServiceApp;
import com.app.derin.uaa.config.SecurityBeanOverrideConfiguration;
import com.app.derin.uaa.domain.UaaRoles;
import com.app.derin.uaa.repository.UaaRolesRepository;
import com.app.derin.uaa.service.UaaRolesService;
import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.service.mapper.UaaRolesMapper;
import com.app.derin.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.app.derin.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UaaRolesResource} REST controller.
 */
@SpringBootTest(classes = AuthServiceApp.class)
public class UaaRolesResourceIT {

    private static final String DEFAULT_ROLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ROLE_NAME = "BBBBBBBBBB";

    @Autowired
    private UaaRolesRepository uaaRolesRepository;

    @Mock
    private UaaRolesRepository uaaRolesRepositoryMock;

    @Autowired
    private UaaRolesMapper uaaRolesMapper;

    @Mock
    private UaaRolesService uaaRolesServiceMock;

    @Autowired
    private UaaRolesService uaaRolesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUaaRolesMockMvc;

    private UaaRoles uaaRoles;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UaaRolesResource uaaRolesResource = new UaaRolesResource(uaaRolesService);
        this.restUaaRolesMockMvc = MockMvcBuilders.standaloneSetup(uaaRolesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaRoles createEntity(EntityManager em) {
        UaaRoles uaaRoles = new UaaRoles()
            .roleName(DEFAULT_ROLE_NAME);
        return uaaRoles;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaRoles createUpdatedEntity(EntityManager em) {
        UaaRoles uaaRoles = new UaaRoles()
            .roleName(UPDATED_ROLE_NAME);
        return uaaRoles;
    }

    @BeforeEach
    public void initTest() {
        uaaRoles = createEntity(em);
    }

    @Test
    @Transactional
    public void createUaaRoles() throws Exception {
        int databaseSizeBeforeCreate = uaaRolesRepository.findAll().size();

        // Create the UaaRoles
        UaaRolesDTO uaaRolesDTO = uaaRolesMapper.toDto(uaaRoles);
        restUaaRolesMockMvc.perform(post("/api/uaa-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaRolesDTO)))
            .andExpect(status().isCreated());

        // Validate the UaaRoles in the database
        List<UaaRoles> uaaRolesList = uaaRolesRepository.findAll();
        assertThat(uaaRolesList).hasSize(databaseSizeBeforeCreate + 1);
        UaaRoles testUaaRoles = uaaRolesList.get(uaaRolesList.size() - 1);
        assertThat(testUaaRoles.getRoleName()).isEqualTo(DEFAULT_ROLE_NAME);
    }

    @Test
    @Transactional
    public void createUaaRolesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uaaRolesRepository.findAll().size();

        // Create the UaaRoles with an existing ID
        uaaRoles.setId(1L);
        UaaRolesDTO uaaRolesDTO = uaaRolesMapper.toDto(uaaRoles);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUaaRolesMockMvc.perform(post("/api/uaa-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaRolesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaRoles in the database
        List<UaaRoles> uaaRolesList = uaaRolesRepository.findAll();
        assertThat(uaaRolesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUaaRoles() throws Exception {
        // Initialize the database
        uaaRolesRepository.saveAndFlush(uaaRoles);

        // Get all the uaaRolesList
        restUaaRolesMockMvc.perform(get("/api/uaa-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uaaRoles.getId().intValue())))
            .andExpect(jsonPath("$.[*].roleName").value(hasItem(DEFAULT_ROLE_NAME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllUaaRolesWithEagerRelationshipsIsEnabled() throws Exception {
        UaaRolesResource uaaRolesResource = new UaaRolesResource(uaaRolesServiceMock);
        when(uaaRolesServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restUaaRolesMockMvc = MockMvcBuilders.standaloneSetup(uaaRolesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUaaRolesMockMvc.perform(get("/api/uaa-roles?eagerload=true"))
        .andExpect(status().isOk());

        verify(uaaRolesServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllUaaRolesWithEagerRelationshipsIsNotEnabled() throws Exception {
        UaaRolesResource uaaRolesResource = new UaaRolesResource(uaaRolesServiceMock);
            when(uaaRolesServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restUaaRolesMockMvc = MockMvcBuilders.standaloneSetup(uaaRolesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUaaRolesMockMvc.perform(get("/api/uaa-roles?eagerload=true"))
        .andExpect(status().isOk());

            verify(uaaRolesServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getUaaRoles() throws Exception {
        // Initialize the database
        uaaRolesRepository.saveAndFlush(uaaRoles);

        // Get the uaaRoles
        restUaaRolesMockMvc.perform(get("/api/uaa-roles/{id}", uaaRoles.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(uaaRoles.getId().intValue()))
            .andExpect(jsonPath("$.roleName").value(DEFAULT_ROLE_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingUaaRoles() throws Exception {
        // Get the uaaRoles
        restUaaRolesMockMvc.perform(get("/api/uaa-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUaaRoles() throws Exception {
        // Initialize the database
        uaaRolesRepository.saveAndFlush(uaaRoles);

        int databaseSizeBeforeUpdate = uaaRolesRepository.findAll().size();

        // Update the uaaRoles
        UaaRoles updatedUaaRoles = uaaRolesRepository.findById(uaaRoles.getId()).get();
        // Disconnect from session so that the updates on updatedUaaRoles are not directly saved in db
        em.detach(updatedUaaRoles);
        updatedUaaRoles
            .roleName(UPDATED_ROLE_NAME);
        UaaRolesDTO uaaRolesDTO = uaaRolesMapper.toDto(updatedUaaRoles);

        restUaaRolesMockMvc.perform(put("/api/uaa-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaRolesDTO)))
            .andExpect(status().isOk());

        // Validate the UaaRoles in the database
        List<UaaRoles> uaaRolesList = uaaRolesRepository.findAll();
        assertThat(uaaRolesList).hasSize(databaseSizeBeforeUpdate);
        UaaRoles testUaaRoles = uaaRolesList.get(uaaRolesList.size() - 1);
        assertThat(testUaaRoles.getRoleName()).isEqualTo(UPDATED_ROLE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingUaaRoles() throws Exception {
        int databaseSizeBeforeUpdate = uaaRolesRepository.findAll().size();

        // Create the UaaRoles
        UaaRolesDTO uaaRolesDTO = uaaRolesMapper.toDto(uaaRoles);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUaaRolesMockMvc.perform(put("/api/uaa-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaRolesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaRoles in the database
        List<UaaRoles> uaaRolesList = uaaRolesRepository.findAll();
        assertThat(uaaRolesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUaaRoles() throws Exception {
        // Initialize the database
        uaaRolesRepository.saveAndFlush(uaaRoles);

        int databaseSizeBeforeDelete = uaaRolesRepository.findAll().size();

        // Delete the uaaRoles
        restUaaRolesMockMvc.perform(delete("/api/uaa-roles/{id}", uaaRoles.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UaaRoles> uaaRolesList = uaaRolesRepository.findAll();
        assertThat(uaaRolesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
