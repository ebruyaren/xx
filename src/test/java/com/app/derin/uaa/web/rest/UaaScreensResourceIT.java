package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.AuthServiceApp;
import com.app.derin.uaa.config.SecurityBeanOverrideConfiguration;
import com.app.derin.uaa.domain.UaaScreens;
import com.app.derin.uaa.repository.UaaScreensRepository;
import com.app.derin.uaa.service.UaaScreensService;
import com.app.derin.uaa.service.dto.UaaScreensDTO;
import com.app.derin.uaa.service.mapper.UaaScreensMapper;
import com.app.derin.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.app.derin.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UaaScreensResource} REST controller.
 */
@SpringBootTest(classes = AuthServiceApp.class)
public class UaaScreensResourceIT {

    private static final String DEFAULT_SCREEN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SCREEN_NAME = "BBBBBBBBBB";

    @Autowired
    private UaaScreensRepository uaaScreensRepository;

    @Mock
    private UaaScreensRepository uaaScreensRepositoryMock;

    @Autowired
    private UaaScreensMapper uaaScreensMapper;

    @Mock
    private UaaScreensService uaaScreensServiceMock;

    @Autowired
    private UaaScreensService uaaScreensService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUaaScreensMockMvc;

    private UaaScreens uaaScreens;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UaaScreensResource uaaScreensResource = new UaaScreensResource(uaaScreensService);
        this.restUaaScreensMockMvc = MockMvcBuilders.standaloneSetup(uaaScreensResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaScreens createEntity(EntityManager em) {
        UaaScreens uaaScreens = new UaaScreens()
            .screenName(DEFAULT_SCREEN_NAME);
        return uaaScreens;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaScreens createUpdatedEntity(EntityManager em) {
        UaaScreens uaaScreens = new UaaScreens()
            .screenName(UPDATED_SCREEN_NAME);
        return uaaScreens;
    }

    @BeforeEach
    public void initTest() {
        uaaScreens = createEntity(em);
    }

    @Test
    @Transactional
    public void createUaaScreens() throws Exception {
        int databaseSizeBeforeCreate = uaaScreensRepository.findAll().size();

        // Create the UaaScreens
        UaaScreensDTO uaaScreensDTO = uaaScreensMapper.toDto(uaaScreens);
        restUaaScreensMockMvc.perform(post("/api/uaa-screens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaScreensDTO)))
            .andExpect(status().isCreated());

        // Validate the UaaScreens in the database
        List<UaaScreens> uaaScreensList = uaaScreensRepository.findAll();
        assertThat(uaaScreensList).hasSize(databaseSizeBeforeCreate + 1);
        UaaScreens testUaaScreens = uaaScreensList.get(uaaScreensList.size() - 1);
        assertThat(testUaaScreens.getScreenName()).isEqualTo(DEFAULT_SCREEN_NAME);
    }

    @Test
    @Transactional
    public void createUaaScreensWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uaaScreensRepository.findAll().size();

        // Create the UaaScreens with an existing ID
        uaaScreens.setId(1L);
        UaaScreensDTO uaaScreensDTO = uaaScreensMapper.toDto(uaaScreens);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUaaScreensMockMvc.perform(post("/api/uaa-screens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaScreensDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaScreens in the database
        List<UaaScreens> uaaScreensList = uaaScreensRepository.findAll();
        assertThat(uaaScreensList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUaaScreens() throws Exception {
        // Initialize the database
        uaaScreensRepository.saveAndFlush(uaaScreens);

        // Get all the uaaScreensList
        restUaaScreensMockMvc.perform(get("/api/uaa-screens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uaaScreens.getId().intValue())))
            .andExpect(jsonPath("$.[*].screenName").value(hasItem(DEFAULT_SCREEN_NAME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllUaaScreensWithEagerRelationshipsIsEnabled() throws Exception {
        UaaScreensResource uaaScreensResource = new UaaScreensResource(uaaScreensServiceMock);
        when(uaaScreensServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restUaaScreensMockMvc = MockMvcBuilders.standaloneSetup(uaaScreensResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUaaScreensMockMvc.perform(get("/api/uaa-screens?eagerload=true"))
        .andExpect(status().isOk());

        verify(uaaScreensServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllUaaScreensWithEagerRelationshipsIsNotEnabled() throws Exception {
        UaaScreensResource uaaScreensResource = new UaaScreensResource(uaaScreensServiceMock);
            when(uaaScreensServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restUaaScreensMockMvc = MockMvcBuilders.standaloneSetup(uaaScreensResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restUaaScreensMockMvc.perform(get("/api/uaa-screens?eagerload=true"))
        .andExpect(status().isOk());

            verify(uaaScreensServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getUaaScreens() throws Exception {
        // Initialize the database
        uaaScreensRepository.saveAndFlush(uaaScreens);

        // Get the uaaScreens
        restUaaScreensMockMvc.perform(get("/api/uaa-screens/{id}", uaaScreens.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(uaaScreens.getId().intValue()))
            .andExpect(jsonPath("$.screenName").value(DEFAULT_SCREEN_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingUaaScreens() throws Exception {
        // Get the uaaScreens
        restUaaScreensMockMvc.perform(get("/api/uaa-screens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUaaScreens() throws Exception {
        // Initialize the database
        uaaScreensRepository.saveAndFlush(uaaScreens);

        int databaseSizeBeforeUpdate = uaaScreensRepository.findAll().size();

        // Update the uaaScreens
        UaaScreens updatedUaaScreens = uaaScreensRepository.findById(uaaScreens.getId()).get();
        // Disconnect from session so that the updates on updatedUaaScreens are not directly saved in db
        em.detach(updatedUaaScreens);
        updatedUaaScreens
            .screenName(UPDATED_SCREEN_NAME);
        UaaScreensDTO uaaScreensDTO = uaaScreensMapper.toDto(updatedUaaScreens);

        restUaaScreensMockMvc.perform(put("/api/uaa-screens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaScreensDTO)))
            .andExpect(status().isOk());

        // Validate the UaaScreens in the database
        List<UaaScreens> uaaScreensList = uaaScreensRepository.findAll();
        assertThat(uaaScreensList).hasSize(databaseSizeBeforeUpdate);
        UaaScreens testUaaScreens = uaaScreensList.get(uaaScreensList.size() - 1);
        assertThat(testUaaScreens.getScreenName()).isEqualTo(UPDATED_SCREEN_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingUaaScreens() throws Exception {
        int databaseSizeBeforeUpdate = uaaScreensRepository.findAll().size();

        // Create the UaaScreens
        UaaScreensDTO uaaScreensDTO = uaaScreensMapper.toDto(uaaScreens);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUaaScreensMockMvc.perform(put("/api/uaa-screens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaScreensDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaScreens in the database
        List<UaaScreens> uaaScreensList = uaaScreensRepository.findAll();
        assertThat(uaaScreensList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUaaScreens() throws Exception {
        // Initialize the database
        uaaScreensRepository.saveAndFlush(uaaScreens);

        int databaseSizeBeforeDelete = uaaScreensRepository.findAll().size();

        // Delete the uaaScreens
        restUaaScreensMockMvc.perform(delete("/api/uaa-screens/{id}", uaaScreens.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UaaScreens> uaaScreensList = uaaScreensRepository.findAll();
        assertThat(uaaScreensList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
