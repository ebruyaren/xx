package com.app.derin.uaa.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class UaaRolesMapperTest {

    private UaaRolesMapper uaaRolesMapper;

    @BeforeEach
    public void setUp() {
        uaaRolesMapper = new UaaRolesMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(uaaRolesMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uaaRolesMapper.fromId(null)).isNull();
    }
}
