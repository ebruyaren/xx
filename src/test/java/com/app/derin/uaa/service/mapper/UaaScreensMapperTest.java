package com.app.derin.uaa.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class UaaScreensMapperTest {

    private UaaScreensMapper uaaScreensMapper;

    @BeforeEach
    public void setUp() {
        uaaScreensMapper = new UaaScreensMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(uaaScreensMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uaaScreensMapper.fromId(null)).isNull();
    }
}
