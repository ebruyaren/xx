package com.app.derin.uaa.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaUsersDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaUsersDTO.class);
        UaaUsersDTO uaaUsersDTO1 = new UaaUsersDTO();
        uaaUsersDTO1.setId(1L);
        UaaUsersDTO uaaUsersDTO2 = new UaaUsersDTO();
        assertThat(uaaUsersDTO1).isNotEqualTo(uaaUsersDTO2);
        uaaUsersDTO2.setId(uaaUsersDTO1.getId());
        assertThat(uaaUsersDTO1).isEqualTo(uaaUsersDTO2);
        uaaUsersDTO2.setId(2L);
        assertThat(uaaUsersDTO1).isNotEqualTo(uaaUsersDTO2);
        uaaUsersDTO1.setId(null);
        assertThat(uaaUsersDTO1).isNotEqualTo(uaaUsersDTO2);
    }
}
