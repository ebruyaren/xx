package com.app.derin.uaa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaScreensTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaScreens.class);
        UaaScreens uaaScreens1 = new UaaScreens();
        uaaScreens1.setId(1L);
        UaaScreens uaaScreens2 = new UaaScreens();
        uaaScreens2.setId(uaaScreens1.getId());
        assertThat(uaaScreens1).isEqualTo(uaaScreens2);
        uaaScreens2.setId(2L);
        assertThat(uaaScreens1).isNotEqualTo(uaaScreens2);
        uaaScreens1.setId(null);
        assertThat(uaaScreens1).isNotEqualTo(uaaScreens2);
    }
}
