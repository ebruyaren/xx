package com.app.derin.uaa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaUsersTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaUsers.class);
        UaaUsers uaaUsers1 = new UaaUsers();
        uaaUsers1.setId(1L);
        UaaUsers uaaUsers2 = new UaaUsers();
        uaaUsers2.setId(uaaUsers1.getId());
        assertThat(uaaUsers1).isEqualTo(uaaUsers2);
        uaaUsers2.setId(2L);
        assertThat(uaaUsers1).isNotEqualTo(uaaUsers2);
        uaaUsers1.setId(null);
        assertThat(uaaUsers1).isNotEqualTo(uaaUsers2);
    }
}
